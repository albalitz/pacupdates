mod news;
mod packages;

pub use news::{get_news, News};
pub use packages::{checkupdates, pacman_syu, Package};
