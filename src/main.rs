use std::io::{self, Write};
use std::process;

const NEWS_COUNT: u8 = 5;

fn gather_data() -> (Vec<pacupdates::Package>, Vec<pacupdates::News>) {
    (
        pacupdates::checkupdates(),
        pacupdates::get_news(Some(NEWS_COUNT)),
    )
}

fn delimiter_line(length: usize) -> String {
    format!("{}", "=".repeat(length))
}

fn main() {
    let (packages, news_items) = gather_data();

    if packages.is_empty() {
        println!("No updates available");
        process::exit(0);
    }

    let head = format!("{} available updates:", packages.len());
    let _delimiter_line = delimiter_line(head.len());

    println!("{}", head);
    println!("\n{}\n", _delimiter_line);
    for package in packages {
        println!("{}", package);
    }
    println!("\n{}\n", _delimiter_line);

    println!("News:");
    for n in news_items {
        println!("{}", n);
    }
    println!("\n{}\n", _delimiter_line);

    let mut input = String::new();
    loop {
        print!("Run `sudo pacman -Syu`? [y/N] ");
        io::stdout().flush().unwrap();
        match io::stdin().read_line(&mut input) {
            Ok(_) => {}
            Err(e) => {
                eprintln!("Error: {}", e);
                process::exit(1);
            }
        }
        if input.trim().to_lowercase() == "y" {
            pacupdates::pacman_syu();
            break;
        } else if input.trim().to_lowercase() == "n" || input == "\n" {
            break;
        }
    }
}
