use std::fmt;
use std::process::{exit, Command, Output};

use regex::Regex;

pub struct Package {
    name: String,
    version_old: String,
    version_new: String,
}

impl fmt::Display for Package {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{} ({} -> {})",
            self.name, self.version_old, self.version_new
        )
    }
}

pub fn pacman_syu() {
    let mut child = Command::new("sudo")
        .arg("pacman")
        .arg("-Syu")
        .spawn()
        .expect("failed to execute child");
    let exit_code = child.wait().expect("failed to wait on child");
    if !exit_code.success() {
        match exit_code.code() {
            Some(c) => exit(c),
            None => exit(1),
        };
    }
}

pub fn checkupdates() -> Vec<Package> {
    let output: String = match checkupdates_cmd() {
        Some(o) => o,
        None => return Vec::new(),
    };
    let updates: Vec<Package> = parse_checkupdates(output);
    return updates;
}

fn checkupdates_cmd() -> Option<String> {
    let available_updates: Result<Output, std::io::Error> = Command::new("checkupdates").output();
    match available_updates {
        Ok(output) => {
            return Some(String::from_utf8_lossy(&output.stdout).to_string());
        }
        Err(e) => {
            eprintln!("{}", e);
            exit(2);
        }
    }
}

fn parse_checkupdates(output: String) -> Vec<Package> {
    let re = Regex::new(r"(.+) (.+) -> (.+)").unwrap();
    let packages: Vec<Package> = re
        .captures_iter(&output)
        .map(|m| Package {
            name: m[1].to_string(),
            version_old: m[2].to_string(),
            version_new: m[3].to_string(),
        })
        .collect();

    return packages;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_parse_package_updates_from_checkupdates_output() {
        let output: String = String::from(
            "package-1 42.9001-1 -> 42.9002-1
package-2 1337-1 -> 1337-2",
        );
        let result: Vec<Package> = parse_checkupdates(output);

        assert_eq!(result.len(), 2);
        assert_eq!(result[0].name, "package-1".to_string());
        assert_eq!(result[0].version_old, "42.9001-1".to_string());
        assert_eq!(result[0].version_new, "42.9002-1".to_string());
        assert_eq!(result[1].name, "package-2".to_string());
        assert_eq!(result[1].version_old, "1337-1".to_string());
        assert_eq!(result[1].version_new, "1337-2".to_string());
    }
}
