use std::fmt;

use scraper::{Html, Selector};

pub struct News {
    published: String,
    title: String,
    url: String,
}

impl fmt::Display for News {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}: {}\n  {}", self.published, self.title, self.url)
    }
}

impl News {
    fn new(published: String, title: String, url: String) -> News {
        return News {
            published: published,
            title: title,
            url: format!("https://www.archlinux.org{}", url),
        };
    }
}

pub fn get_news(count: Option<u8>) -> Vec<News> {
    let url = "https://www.archlinux.org/news/";

    let client = reqwest::Client::new();
    let mut response = match client.get(url).send() {
        Ok(r) => r,
        Err(e) => {
            eprintln!("Error: {}", e);
            return Vec::new();
        }
    };

    let news: Vec<News> = match response.text() {
        Ok(content) => find_news_in_html(content, count),
        Err(_) => {
            println!(
                "Error parsing json response. HTTP status code: {}",
                response.status()
            );
            return Vec::new();
        }
    };
    return news;
}

fn find_news_in_html(html: String, count: Option<u8>) -> Vec<News> {
    let table_selector = Selector::parse("#article-list").unwrap();
    let tr_selector = Selector::parse("tr").unwrap();
    let td_selector = Selector::parse("td").unwrap();
    let a_selector = Selector::parse("a").unwrap();

    let document = Html::parse_document(&html);
    let table = document.select(&table_selector).next().unwrap();
    let mut news: Vec<News> = Vec::new();
    for tr in table.select(&tr_selector) {
        let mut tds: scraper::element_ref::Select = tr.select(&td_selector);
        let published: String = match tds.next() {
            Some(e) => e.inner_html(),
            None => continue,
        };
        let title_td = match tds.next() {
            Some(e) => e,
            None => continue,
        };
        let title: String = match title_td.select(&a_selector).next() {
            Some(a) => a.inner_html(),
            None => continue,
        };
        let url: String = match title_td.select(&a_selector).next() {
            Some(a) => a.value().attr("href").unwrap().to_string(),
            None => continue,
        };
        news.push(News::new(published, title, url));

        match count {
            Some(c) => {
                if news.len() == usize::from(c) {
                    break;
                }
            }
            None => {}
        }
    }
    return news;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_find_news_in_html() {
        let html = String::from(r#"<html><body><table id="article-list" class="results">
        <thead>
            <tr>
                <th>Published</th>
                <th>Title</th>
                <th>Author</th>

            </tr>
        </thead>
        <tbody>
            <tr class="odd">
                <td>2018-07-14</td>
                <td class="wrap"><a href="/news/libutf8proc211-3-update-requires-manual-intervention/" title="View: libutf8proc>=2.1.1-3 update requires manual intervention">libutf8proc&gt;=2.1.1-3 update requires manual intervention</a></td>
                <td>Antonio Rojas</td>
            </tr>

            <tr class="even">
                <td>2018-05-04</td>
                <td class="wrap"><a href="/news/js52-5273-2-upgrade-requires-intervention/" title="View: js52 52.7.3-2 upgrade requires intervention">js52 52.7.3-2 upgrade requires intervention</a></td>
                <td>Jan Steffens</td>
            </tr>
        </tbody>
    </table></body></html>"#);
        let news: Vec<News> = find_news_in_html(html, None);

        assert_eq!(news.len(), 2);
        assert_eq!(
            news[0].title,
            "libutf8proc&gt;=2.1.1-3 update requires manual intervention".to_string()
        );
    }

    #[test]
    fn should_find_news_online() {
        let result: Vec<News> = get_news(Some(5));

        assert_eq!(result.len(), 5);
    }
}
