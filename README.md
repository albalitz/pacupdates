# PacUpdates
A tool for checking `pacman` package updates and [Arch Linux news](https://www.archlinux.org/news/) before updating.

Inspired by a [script I found on reddit](https://www.reddit.com/r/archlinux/comments/a6tl32/bash_script_that_wraps_the_output_of_checkupdates/).

You can build this using `cargo build --release`, or you can download precompiled binaries from the [tags page](https://gitlab.com/albalitz/pacupdates/tags).

## Dependencies
Apart from the Rust dependencies installed via `cargo`, you'll need to have `checkupdates` (from [`pacman-contrib`](https://www.archlinux.org/packages/?name=pacman-contrib)) installed.
